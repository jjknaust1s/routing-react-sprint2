import React from 'react';
import {Route, Switch} from "react-router-dom"
import './App.css';
import Welcome from "./components/welcome/Welcome"
import Clock from "./components/clock/Clock"
import Contact from "./components/contact/Contact"
import Navigation from "./components/navigation/Navigation"
import NotFound from "./components/notFound/NotFound"

function App() {
  return (
    <div className="App">
      <Navigation />
      <Switch>
      <Route exact path= '/'render= {(props)=> <Welcome  name ='Jacob'/>}/>
      <Route exact path= '/welcome'render= {(props)=> <Welcome  name ='Jacob'/>}/>
      <Route 
      exact 
      path='/welcome/:name' 
      render= {(props)=> <Welcome  name ={props.match.params.name} />}/>
      <Route path='/clock' component = {Clock}/>
      <Route path='/contact' component = {Contact}/>
      <Route path ="*">
        <NotFound />
      </Route>
  </Switch>
    </div>
  );
}

export default App;